<?php
/**
 * Enqueue scripts and styles for the front end.
 *
 * @return void
 */
function theme_scripts()
{

	// Google Fonts
	wp_enqueue_style('fonts','//fonts.googleapis.com/css?family=Roboto:400,900italic,700italic,900,700,500italic,500,400italic,300italic,300,100italic,100|Open+Sans:400,300,400italic,300italic,600,600italic,700italic,700,800|Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700');

	// JQuery
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');

	// Load our main stylesheet.
	wp_enqueue_style('style', get_stylesheet_uri());

	// Styles
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/dist/styles/main.min.css');

	// Scripts
	wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/dist/js/plugins.min.js', array('jquery'), '1.0.0', true );

	wp_enqueue_script('main', get_template_directory_uri() . '/assets/dist/js/main.js', '1.0.0', false );

	// Ajax
	wp_localize_script('scripts', 'globalAjax', array('ajaxurl'=>admin_url('admin-ajax.php'),'nonce' => wp_create_nonce('ajax-nonce')));


}

add_action('wp_enqueue_scripts', 'theme_scripts');
