<?php

// Remove menus
function remove_menus(){

  remove_menu_page( 'edit.php' );                   //Posts Orginal
  remove_menu_page( 'edit-comments.php' );          //Comments

}
add_action( 'admin_menu', 'remove_menus' );





//  Hide Adminbar
add_filter('show_admin_bar', '__return_false');






// enable svg images

function custom_mtypes( $m ){
    $m['svg'] = 'image/svg+xml';
    return $m;
}
add_filter( 'upload_mimes', 'custom_mtypes' );





// Add theme featured image theme support

add_theme_support( 'post-thumbnails' );
function thumb_setup()
{
    add_image_size( 'gallery', 1400, 480, true ); // Hard crop to exact dimensions (crops sides or top and bottom)
    add_image_size( 'palfestivalian', 95, 95, true ); // Hard crop to exact dimensions (crops sides or top and bottom)
    add_image_size( 'book-cover', 300, 300, true ); // Hard crop to exact dimensions (crops sides or top and bottom)

}
add_action( 'after_setup_theme', 'thumb_setup' );





//  Theme Custom Menu Support


function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );


add_filter( 'wp_nav_menu_items', 'more_links', 10, 2 );



function more_links( $items, $args )
{
	$url    = get_field('more_links','options');
	$img 		= get_template_directory_uri() .'/assets/images/arabic.png';
  $items .= '<li><a href="'. $url .'" target="_blank" class="arabic-lang" style="background-color: transparent !important;"><img src="'. $img .'"" alt=""></a></li>
            <li><a href="'. $url .'" target="_blank"  id="twitter" class="twitter" style="margin-top:12px !important; background-color: transparent !important;"></a></li>
            <li><a href="'. $url .'" target="_blank" class="facebook" style="margin-top:12px !important; background-color: transparent !important;"></a></li>
            <li><a href="'. $url .'" target="_blank" class="youtube" style="margin-top:12px !important; background-color: transparent !important;"></a></li>';
    return $items;
}


/**
 * Dfault posts type url structure (BLOG)
 */
function my_new_default_post_type()
{

    register_post_type('post', array(
        'labels' => array(
            'name_admin_bar' => _x('Post', 'add new on admin bar')
        ),
        'public' => true,
        '_builtin' => false,
        '_edit_link' => 'post.php?post=%d',
        'menu_position' => 2,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array(
            'slug' => 'Author-Blog'
        ),
        'has_archive' => true,
        'query_var' => false,
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'post-formats'
        )
    ));
}
add_action('init', 'my_new_default_post_type', 1);
