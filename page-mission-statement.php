<?php get_header(); ?>
<div class="container mission-container">
    <div class="row">
        <div class="col-md-12 content">
            <?php while( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
