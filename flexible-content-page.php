<?php

/*
Template Name: Flexible Content Page
*/

?>
<?php get_header(); ?>
<div class="container">
	<?php while (the_flexible_field("flexible_content"))
	{
		if (get_row_layout() == "one_column")
		{
			$one = get_sub_field('one_column');
		?>
			<div class="row">
				<div class="col-md-12 text-center">
					<?php
						echo $one;
					?>
				</div>
			</div>
		<?php
		}
		if (get_row_layout() == "two_columns")
		{
			$oneofTwo = get_sub_field('one_of_two');
			$twoofTwo = get_sub_field('two_of_two');
		?>
			<div class="row">
				<div class="col-md-6 text-center">
					<?php
						echo $oneofTwo;
					?>
				</div>
				<div class="col-md-6 text-center">
					<?php
						echo $twoofTwo;
					?>
				</div>
			</div>
		<?php
		}
		if (get_row_layout() == "three_columns")
		{
			$oneofThree = get_sub_field('one_of_three');
			$twoofThree = get_sub_field('two_of_three');
			$threeofThree = get_sub_field('three_of_three');
		?>
			<div class="row">
				<div class="col-md-4 text-center">
					<?php
						echo $oneofThree;
					?>
				</div>
				<div class="col-md-4 text-center">
					<?php
						echo $twoofThree;
					?>
				</div>
				<div class="col-md-4 text-center">
					<?php
						echo $threefThree;
					?>
				</div>
			</div>
		<?php
		}
		if (get_row_layout() == "four_columns")
		{
			$oneofFour = get_sub_field('one_of_four');
			$twoofFour = get_sub_field('two_of_four');
			$threeofFour = get_sub_field('three_of_four');
			$fourofFour = get_sub_field('four_of_four');
		?>
			<div class="row">
				<div class="col-md-3 text-center">
					<?php
						echo $oneofFour;
					?>
				</div>
				<div class="col-md-3 text-center">
					<?php
						echo $twoofFour;
					?>
				</div>
				<div class="col-md-3 text-center">
					<?php
						echo $threeofFour;
					?>
				</div>
				<div class="col-md-3 text-center">
					<?php
						echo $fourofFour;
					?>
				</div>
			</div>
		<?php
		}
	}
	?>
</div>
<?php get_footer(); ?>

