<?php

function palfestivalians() {

  $labels = array(
    'name'               => _x( 'Palfestivalians', 'Palfestivalians' ),
    'singular_name'      => _x( 'Palfestivalian', 'Palfestivalian' ),
    'add_new'            => _x( 'Add New', 'Palfestivalian' ),
    'add_new_item'       => __( 'Add New Palfestivalian' ),
    'edit_item'          => __( 'Edit Palfestivalian' ),
    'new_item'           => __( 'New Palfestivalian' ),
    'all_items'          => __( 'All Palfestivalians' ),
    'view_item'          => __( 'View Palfestivalians' ),
    'search_items'       => __( 'Search Palfestivalians' ),
    'not_found'          => __( 'No Palfestivalian found' ),
    'not_found_in_trash' => __( 'No Floors found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Palfestivalians'


  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Palfestivalians',
    'public'        => true,
    'menu_position' => 5,
    'has_archive'   => true,
    'hierarchical' => true,
    'supports'      => array('title','thumbnail'),

  );
  register_post_type( 'palfestivalians', $args );
}
add_action( 'init', 'palfestivalians' ); ?>
