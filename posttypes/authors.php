<?php

function authors() {

  $labels = array(
    'name'               => _x( 'Authors', 'authors' ),
    'singular_name'      => _x( 'Author', 'Author' ),
    'add_new'            => _x( 'Add New', 'Author' ),
    'add_new_item'       => __( 'Add New Author' ),
    'edit_item'          => __( 'Edit Author' ),
    'new_item'           => __( 'New Author' ),
    'all_items'          => __( 'All Authors' ),
    'view_item'          => __( 'View Authors' ),
    'search_items'       => __( 'Search Authors' ),
    'not_found'          => __( 'No Author found' ),
    'not_found_in_trash' => __( 'No Floors found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Authors'


  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Authors',
    'public'        => true,
    'menu_position' => 4,
    'has_archive'   => true,
    'hierarchical' => true,
    'supports'      => array('title','thumbnail'),

  );
  register_post_type( 'authors', $args );
}
add_action( 'init', 'authors' ); ?>
