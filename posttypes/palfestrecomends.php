<?php

function palfestRecommends() {

  $labels = array(
    'name'               => _x( 'Palfest Recommends', 'Palfest Recommends' ),
    'singular_name'      => _x( 'Palfest Recomendation', 'Palfest Recomendation' ),
    'add_new'            => _x( 'Add New', 'Palfest Recomendation' ),
    'add_new_item'       => __( 'Add New Palfest Recomendation' ),
    'edit_item'          => __( 'Edit Palfest Recomendation' ),
    'new_item'           => __( 'New Palfest Recomendation' ),
    'all_items'          => __( 'All Palfest Recommends' ),
    'view_item'          => __( 'View Palfest Recommends' ),
    'search_items'       => __( 'Search Palfest Recommends' ),
    'not_found'          => __( 'No Palfest Recomendations found' ),
    'not_found_in_trash' => __( 'No Palfest Recommendations found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Palfest Recommends'


  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'palfest Recommends',
    'public'        => true,
    'menu_position' => 3,
    'has_archive'   => true,
    'hierarchical' => true,
    'supports'      => array('title'),

  );
  register_post_type( 'palfestRecommends', $args );
}
add_action( 'init', 'palfestRecommends' ); ?>
