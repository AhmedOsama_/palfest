<?php get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 palfestivalian-header text-center">
            <?php
                $headerImage = get_field('palfestivalians_header','options');
                $subHeader  =  get_field('palfestivalians_sub_text','options');
            ?>
            <img src="<?php echo $headerImage['sizes']['large'] ?>" class="img-responsive" alt="palfestivalians_header_image">
            <p>
                <?php echo $subHeader; ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 alphaList text-center">
            <?php

                $args = array(
                  'post_type' => 'palfestivalians',
                  'orderby' => 'title',
                  'order' => 'ASC',
                  'posts_per_page' => -1
                );
                $query = new WP_Query( $args );
                ?>
                <?php $currentLetter = ''; ?>
                <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
                  <?php

                  if($currentLetter != strtoupper(substr(get_the_title(), 0, 1))) :
                    $currentLetter = strtoupper(substr(get_the_title(), 0, 1));
                    if ($currentLetter == $_GET['l'])
                        $selected = 'active-letter';
                    else
                        $selected = '';

                    $string = 'hey';
                  ?>
                        <?php
                        endif;
                      endwhile; endif;
             ?>

            <?php
                    foreach(range('A', 'Z') as $letter):
                        if($_GET['l'] === $letter)
                            $selected = 'active-letter';
                        else
                            $selected = '';
                        ?>
                        <a class="singleLetter <?php echo $selected; ?>" href="?l=<?php echo $letter; ?>" title=""><?php echo $letter; ?></a>
                        <?php
                    endforeach;
            ?>

        </div>
    </div>

        <div class="row">

            <div class="col-md-12">
                <?php
                function filter_where( $where = '' ) {
                    $l = mysql_real_escape_string($_GET['l']);
                    $where .= ' AND (SUBSTR(post_title, 1, 1)) = "'.$l.'"';
                    return $where;
                }

                if (isset($_GET['l'])) {

                    add_filter('posts_where', 'filter_where');
                    $args = array( 'post_type' => 'palfestivalians', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
                    $query = new WP_Query( $args );

                } else {

                    $args = array( 'post_type' => 'palfestivalians', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
                    $query = new WP_Query( $args );

                }
                ?>

                <div class="col-md-12 selectedLetter">
                    <?php if(!isset($_GET['l'])){ ?>
                        <span class="selected-letter"> A-Z </span>
                    <?php } else { ?>
                        <span class="selected-letter"><?php echo $_GET['l'] ?></span>
                    <?php } ?>
                </div>
                <?php if (isset($_GET['l'])): ?>
                    <div class="col-md-2 showAll">
                        <?php wp_reset_query(); ?>
                        <a href="<?php echo get_post_type_archive_link( 'palfestivalians' ); ?>">Show All</a>
                    </div>
                <?php endif ?>
            </div>

        </div>
            <div class="row">
                <div class="col-md-12 text-center">
        <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();  ?>
                    <div class="col-md-3 innerTitle majorpoints" id="<?php echo $post->post_name; ?>">
                        <?php $image = get_field('palfestivalian_image'); ?>
                        <img src="<?php echo $image['sizes']['palfestivalian']; ?>" alt="palfestivalian-image">
                        <div class="pal-name">
                            <?php echo str_replace(' ', ' ', get_the_title()); ?>
                        </div>
                        <div class="hiders" style="display:none" >
                           <p><?php echo get_field('palfestivalian_bio'); ?></p>
                        </div>

                    </div>
            <?php $isEnd = ""; ?>
        <?php endwhile; else: ?>

                <div class="col-md-12 noTermsFound">
                    <div class="col-md-12">
                        <p>No terms found.</p>
                    </div>
                </div>

        <?php endif; wp_reset_query(); ?>
                </div>
            </div>
</div>
<?php get_footer(); ?>
