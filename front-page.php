<?php get_header(); ?>
<div class="container">
    <div class="row">

        <div class="col-lg-3 col-md-12">
          <?php
            $videoslides = get_field('video_slider');
          ?>
          <div id="content-slider-1" class="royalSlider contentSlider rsDefault">
            <?php foreach ($videoslides as $videoslide): ?>
              <?php
                $videoTitle = $videoslide['video_title'];
                $videoLink  = $videoslide['video_link'];
                $videoText  = $videoslide['video_text'];
               ?>
              <div>
                <img class="rsImg rsMainSlideImage" src="<?php echo video_image($videoLink); ?>" data-rsvideo="<?php echo $videoLink; ?>"data-rsw="640" data-rsh="425">
                <h3><?php echo $videoTitle; ?></h3>
                <p><?php echo $videoText; ?></p>
              </div>
            <?php endforeach ?>
          </div>
        </div>

        <div class="col-lg-3 col-md-12">
          <?php
            $imageSlides = get_field('image_slider');
          ?>
          <div id="content-slider-2" class="royalSlider contentSlider rsDefault">
            <?php foreach ($imageSlides as $imageSlide): ?>
              <?php
                $title     = $imageSlide['slide_title'];
                $image      = $imageSlide['slide_image'];
                $text      = $imageSlide['slide_text'];
                $hyperLink = $imageSlide['slide_hyperlink'];
               ?>
              <div>

                <img class="rsImg" src="<?php echo $image['url']; ?>" data-rsw="640" data-rsh="425">
                <h3><?php echo $title; ?></h3>
                <p><?php echo $text; ?></p>
              </div>
            <?php endforeach ?>
          </div>
        </div>

        <div class="col-lg-3 col-md-12">
            <?php
              $articleSlides = get_field('article_slider');
            ?>
            <div id="content-slider-3" class="royalSlider contentSlider rsDefault">
              <?php foreach ($articleSlides as $articleSlide): ?>
                <?php
                  $title     = $articleSlide['article_title'];
                  $image      = $articleSlide['article_image'];
                  $text      = $articleSlide['article_text'];
                  $hyperLink = $articleSlide['article_hyperlink'];
                 ?>
                <div>
                  <img class="rsImg" src="<?php echo $image['url']; ?>" data-rsw="707" data-rsh="425">
                  <h3><?php echo $title; ?></h3>
                  <p><?php echo $text; ?></p>
                </div>
              <?php endforeach ?>
            </div>
        </div>

        <div class="col-lg-3 col-md-12">
            <div id="MainMenu">
              <div class="list-group panel">
                <a href="#demo3" class="list-group-item list-group-item-success text-center" data-toggle="collapse" data-parent="#MainMenu"><i class="twitter-feed fa fa-twitter" aria-hidden="true"></i></a>
                <div class="collapse in" id="demo3">
                  <a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1">Tweet 1</a>
                  <a href="javascript:;" class="list-group-item">Tweet 2</a>
                  <a href="javascript:;" class="list-group-item">Tweet 3</a>
                  <a href="javascript:;" class="list-group-item">Tweet 4</a>
                  <a href="javascript:;" class="list-group-item">Tweet 5</a>
                  <a href="javascript:;" class="list-group-item">Tweet 6</a>
                  <a href="javascript:;" class="list-group-item">Tweet 7</a>
                  <a href="javascript:;" class="list-group-item">Tweet 8</a>
                  <a href="javascript:;" class="list-group-item">Tweet 9</a>
                </div>
                <a href="#demo4" class="list-group-item list-group-item-success text-center" data-toggle="collapse" data-parent="#MainMenu"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                <div class="collapse" id="demo4">
                  <a href="" class="list-group-item">Facebook Post 1</a>
                  <a href="" class="list-group-item">Facebook Post 2</a>
                  <a href="" class="list-group-item">Facebook Post 3</a>
                  <a href="" class="list-group-item">Facebook Post 4</a>
                  <a href="" class="list-group-item">Facebook Post 5</a>
                  <a href="" class="list-group-item">Facebook Post 6</a>
                  <a href="" class="list-group-item">Facebook Post 7</a>
                  <a href="" class="list-group-item">Facebook Post 8</a>
                  <a href="" class="list-group-item">Facebook Post 9</a>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <section class="cd-horizontal-timeline">
          <div class="timeline">
            <div class="events-wrapper">
              <div class="events">
                <ol>
                  <li><a href="#0" data-date="16/01/2014" class="selected">16 Jan</a></li>
                  <li><a href="#0" data-date="28/02/2014">28 Feb</a></li>
                  <li><a href="#0" data-date="20/04/2014">20 Mar</a></li>
                  <li><a href="#0" data-date="20/05/2014">20 May</a></li>
                  <li><a href="#0" data-date="09/07/2014">09 Jul</a></li>
                  <li><a href="#0" data-date="30/08/2014">30 Aug</a></li>
                  <li><a href="#0" data-date="15/09/2014">15 Sep</a></li>
                  <li><a href="#0" data-date="01/11/2014">01 Nov</a></li>
                  <li><a href="#0" data-date="10/12/2014">10 Dec</a></li>
                  <li><a href="#0" data-date="19/01/2015">29 Jan</a></li>
                  <li><a href="#0" data-date="03/03/2015">3 Mar</a></li>
                  <li><a href="#0" data-date="22/04/2015">22 Apr</a></li>
                </ol>

                <span class="filling-line" aria-hidden="true"></span>
              </div> <!-- .events -->
            </div> <!-- .events-wrapper -->

            <ul class="cd-timeline-navigation">
              <li><a href="#0" class="prev inactive">Prev</a></li>
              <li><a href="#0" class="next">Next</a></li>
            </ul> <!-- .cd-timeline-navigation -->
          </div> <!-- .timeline -->

          <div class="events-content">
            <ol>
              <li class="selected" data-date="16/01/2014">
                <p>Event title here</p>
                <em>January 16th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="28/02/2014">
                <p>Event title here</p>
                <em>February 28th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="20/04/2014">
                <p>Event title here</p>
                <em>March 20th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="20/05/2014">
                <p>Event title here</p>
                <em>May 20th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="09/07/2014">
                <p>Event title here</p>
                <em>July 9th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="30/08/2014">
                <p>Event title here</p>
                <em>August 30th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="15/09/2014">
                <p>Event title here</p>
                <em>September 15th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="01/11/2014">
                <p>Event title here</p>
                <em>November 1st, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="10/12/2014">
                <p>Event title here</p>
                <em>December 10th, 2014</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="19/01/2015">
                <p>Event title here</p>
                <em>January 19th, 2015</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="03/03/2015">
                <p>Event title here</p>
                <em>March 3rd, 2015</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>

              <li data-date="22/04/2015">
                <p>Event title here</p>
                <em>April 26th, 2015</em>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.
                </p>
              </li>
            </ol>
          </div> <!-- .events-content -->

        </section>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <span class="palfest-recommends">Palfest Recommends</span>
      </div>
    </div>
    <div class="row">
      <div class="wrapper">
        <ul class="stage clearfix">
        <?php
        $args = array(
            'post_type' => 'palfestRecommends',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page'=> -1,
          );
          $query = new WP_Query( $args );

          if($query->have_posts() ) :
            while( $query->have_posts() ) : $query->the_post();

               $cover  =  get_field('book_cover',$post->ID);
               $author =  get_field('book_author',$post->ID);
               $blurb  = get_field('book_blurb',$post->ID);
             ?>


             <li class="scene">
               <div class="movie" onclick="return true">
                 <div class="poster" style="background-image: url(<?php echo $cover['sizes']['book-cover']; ?>);"></div>
                 <div class="info">
                   <header>
                     <h1><?php echo the_title(); ?></h1>
                     <span class="duration"><?php echo $author; ?></span>
                   </header>
                   <p>
                     <?php echo $blurb; ?>
                   </p>
                 </div>
               </div>
             </li>

            <?php endwhile;
            endif
            ;?>
        </ul>
      </div><!-- /wrapper -->
    </div>
</div>
<?php get_footer(); ?>
