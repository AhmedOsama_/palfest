<?php
// Posttypes
require_once('posttypes/palfestivalians.php');                         // Palfestivalians Posttype
require_once('posttypes/authors.php');                                 // Palfestivalians Posttype
require_once('posttypes/palfestrecomends.php');                        // Palfestivalians Posttype

// Advanced custom fields
include_once('lib/acf/acf-flexible-content/acf-flexible-content.php'); 	// ACF Flexible Content
include_once('lib/acf/acf-options-page/acf-options-page.php');			// ACF Options Page
include_once('lib/acf/acf-repeater/acf-repeater.php');					// ACF Repeater
include_once('lib/acf/advanced-custom-fields/acf.php');                 // ACF
include_once('lib/acf/acf-gallery/acf-gallery.php');					// ACF Gallery
// include_once('lib/acf/fields.php');										// ACF Custom Fields
include_once('lib/filters/acf-menu.php');								// ACF Menu



// Bootstrap
require_once('wp_bootstrap_navwalker.php');								// Walker Menu


// Wodpress
require_once('lib/filters/wordpress.php');								// Wordpress Additions

require_once('lib/custom.php');											// Functions
require_once('lib/scripts.php');										// Theme Scripts
