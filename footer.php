        <?php $logo =  get_field('logo','options'); ?>
		<div id="footer">
			<div class="container footer-margin">
                <a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo $logo['sizes']['large']; ?>" alt="" class="footer-logo"></a>
				<p class="text-muted footer-menu">
                    <a href="#">Change Language</a>
                    <a href="#">Contact Us</a>
                    <a href="#">Site Map</a>
                    <a href="#">Terms and Conditions</a>
                    <a href="#">Privacy</a>
                </p>
			</div>
		</div>
		<?php wp_footer(); ?>

	</body>
</html>




