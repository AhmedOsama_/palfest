// Load plugins

var gulp 		= require('gulp'),
autoprefixer 	= require('gulp-autoprefixer'),
less     		= require('gulp-less'),
minifyCss     	= require('gulp-clean-css'),
livereload     	= require('gulp-livereload'),
rename        	= require('gulp-rename'),
notify 			= require('gulp-notify'),
uglify         	= require('gulp-uglify'),
concat         	= require('gulp-concat'),
cache          	= require('gulp-cache'),
jshint         	= require('gulp-jshint'),
imagemin    	= require('gulp-imagemin'),
lr 				= require('tiny-lr'),
server 			= lr();

// Styles
gulp.task('styles', function() {
  return gulp.src('assets/styles/src/*.less')
	.pipe(less({ style: 'expanded' }))
	.pipe(autoprefixer('last 12 versions', 'ie 9', 'ios 6', 'android 4'))
	.pipe(gulp.dest('assets/styles/dist'))
	.pipe(minifyCss({ keepSpecialComments: 0 }))
	.pipe(livereload(server))
	.pipe(rename({ suffix: '.min' }))
	.pipe(gulp.dest('assets/dist/styles'))
	.pipe(notify({ message: 'Styles task complete' }));
});

// Vendor Plugin Scripts
gulp.task('plugins', function() {
  return gulp.src([
  		'assets/js/vendor/bootstrap/affix.js',
  		'assets/js/vendor/bootstrap/alert.js',
  		'assets/js/vendor/bootstrap/button.js',
  		'assets/js/vendor/bootstrap/collapse.js',
  		'assets/js/vendor/bootstrap/dropdown.js',
  		'assets/js/vendor/bootstrap/modal.js',
  		'assets/js/vendor/bootstrap/scrollspy.js',
  		'assets/js/vendor/bootstrap/tab.js',
  		'assets/js/vendor/bootstrap/tooltip.js',
  		'assets/js/vendor/bootstrap/popover.js',
  		'assets/js/vendor/bootstrap/transition.js',

  		'assets/js/src/plugins.js',
  		'assets/js/vendor/**/*.js'
  	])
	.pipe(concat('plugins.js'))
	.pipe(rename({ suffix: '.min' }))
	.pipe(uglify())
	.pipe(livereload(server))
	.pipe(gulp.dest('assets/dist/js'))
	.pipe(notify({ message: 'Plugins task complete' }));
});

// Site Scripts
gulp.task('scripts', function() {
  return gulp.src(['assets/js/src/*.js', '!assets/js/src/plugins.js'])
	// .pipe(jshint('.jshintrc'))
	// .pipe(jshint.reporter('default'))
	.pipe(concat('main.js'))
	.pipe(gulp.dest('assets/dist/js'))
	.pipe(rename({ suffix: '.min' }))
	.pipe(uglify())
	.pipe(livereload(server))
	.pipe(gulp.dest('assets/dist/js'))
	.pipe(notify({ message: 'Scripts task complete' }));
});

// Images
gulp.task('images', function() {
  return gulp.src('assets/images/**/*')
	.pipe(cache(imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
	.pipe(livereload(server))
	.pipe(gulp.dest('assets/dist/images'))
	.pipe(notify({ message: 'Images task complete' }));
});

// Reload
gulp.task('reload', function() {
	return gulp.src('*.php').pipe(livereload(server));
});

// Watch
gulp.task('watch', function() {

  server.listen(35729, function (err) {
	if (err) {
	  return console.log(err)
	};

	// Watch .less files
	gulp.watch('assets/styles/src/**/*.less', ['styles']);

	// Watch .js files
	gulp.watch('assets/js/**/*.js', ['plugins', 'scripts']);

	// Watch .php files
	gulp.watch(['*.php', '**/*.php'], ['reload']);

	// Watch image files
	gulp.watch('assets/images/**/*', ['images']);

  });

});

// Default task
gulp.task('default', ['styles', 'plugins', 'scripts', 'images', 'reload', 'watch']);
