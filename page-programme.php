<?php get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <span class="programme-title"><?php echo get_field('programme_title'); ?></span>
        </div>
    </div>
    <div class="row">
        <?php
            $programmes = get_field('programme_schedule');
            $counter    = 0;
        ?>
        <?php foreach ($programmes as $programme): ?>
            <?php
                $counter++;
                $proDate        = $programme['date'];
                $prolocation    = $programme['town_city'];
                $proEventTime   = $programme['event_time'];
                $proVenue       = $programme['venue'];
                $proPanel       = $programme['panel'];
                $proVideo       = $programme['video_message'];
                $proEveningTime = $programme['time'];
                $proHonouring   = $programme['honouring'];
                $proReading     = $programme['reading'];
                $proMusic       = $programme['music'];
                $proPerformance = $programme['performance'];
            ?>
            <div class="col-md-2 programme">
                <div class="programme-day text-center">
                    Day&nbsp;<?php echo $counter; ?>
                </div>

                <div class="program-block">
                    <div class="programme-date programme-item"></div>

                    <span class="pro-date"><?php echo date('l jS \of F Y', strtotime( $proDate)); ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-location programme-item"></div>
                    <span class="pro-location"><?php echo $prolocation; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-time programme-item"></div>
                    <span class="pro-time"><?php echo $proEveningTime; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-venue programme-item"></div>
                    <span class="pro-venue"><?php echo $proVenue; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-panel programme-item"></div>
                    <span class="pro-panel"><?php echo $proPanel; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-video programme-item"></div>
                    <span class="pro-video"><?php echo $proVideo; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-evening programme-item"></div>
                    <span class="pro-evening"><?php echo $proEveningTime; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-honouring programme-item"></div>
                    <span class="pro-honouring"><?php echo $proHonouring; ?></span>
                </div>

               <div class="program-block">
                   <div class="programme-reading programme-item"></div>
                   <span class="pro-reading"><?php echo $proReading; ?></span>
               </div>

                <div class="program-block">
                    <div class="programme-music programme-item"></div>
                    <span class="pro-music"><?php echo $proMusic; ?></span>
                </div>

                <div class="program-block">
                    <div class="programme-performance programme-item"></div>
                    <span class="pro-performance"><?php echo $proPerformance; ?></span>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
<?php get_footer(); ?>
