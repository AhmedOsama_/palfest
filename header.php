<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <?php wp_head(); ?>
    </head>
    <?php if (is_page('patrons')): ?>
        <body data-spy="scroll" data-target="#scroll-spy">
    <?php else: ?>
        <body>
    <?php endif ?>

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar" role="navigation">

            <div class="container">

                <div id="logo">
                    <div class="text-center">
                        <?php $logo =  get_field('logo','options'); ?>
                    <a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo $logo['sizes']['large']; ?>" alt=""></a>
                    </div>
                </div>
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#">Starter Theme</a> -->
                </div>

                <div class="navbar-collapse collapse palfest-nav">


                    <?php
                        wp_nav_menu( array(
                            'menu'              => 'header-menu',
                            'theme_location'    => 'header-menu',
                            'depth'             => 3,
                            'container'         => 'ul',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                        );
                    ?>

                </div>


            </div>

        </div>
