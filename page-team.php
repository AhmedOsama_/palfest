<?php get_header(); ?>
<div class="container">
    <div class="row">

        <div class="col-md-4 content team-content">
            <?php
                $team_members1 = get_field('team_members_list_1');
            ?>
            <?php foreach ($team_members1 as $team_member): ?>
                <div class="col-md-12 member-block">
                    <div class="col-md-7">
                        <div class="member-title">
                            <?php echo $team_member['team_title']; ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="member-name">
                            <?php echo $team_member['team_name']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

        <div class="col-md-4 content team-content">
            <?php
                $team_members2 = get_field('second_row_of_members');
            ?>
            <?php foreach ($team_members2 as $team_member): ?>
                <div class="col-md-12 member-block">
                    <div class="col-md-5">
                        <div class="member-title">
                            <?php echo $team_member['team_title']; ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="member-name">
                            <?php echo $team_member['team_name']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

        <div class="col-md-4 content team-content">
            <?php
                $team_members3 = get_field('third_column_of_members');
            ?>
            <?php foreach ($team_members3 as $team_member): ?>
                <div class="col-md-12 member-block">
                    <div class="col-md-4">
                        <div class="member-title">
                            <?php echo $team_member['team_title']; ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="member-name">
                            <?php echo $team_member['team_name']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

    </div>
</div>
<?php get_footer(); ?>
